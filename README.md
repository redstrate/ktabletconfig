# ktabletconfig

A CLI utility to change tablet configuration on the [KDE Plasma Desktop](https://kde.org/plasma-desktop). This is meant to be used in situations not supported by the Drawing Tablet KCM, such as a automation or touching experimental settings not yet exposed in the GUI. **In most cases, you can do the same configuration in the KCM :-)**

This tool is similar to [gsetwacom for GNOME](https://github.com/linuxwacom/gsetwacom) or [xsetwacom used on the Plasma X11 session](https://github.com/linuxwacom/xf86-input-wacom).

## Usage

```shell
Usage: ktabletconfig [options]
Configure tablets from the CLI

Options:
  --list-stylus                     List connected tablet stylus.
  --list-pad                        List connected tablet pads.
  --stylus <name>                   Set the name of the tablet stylus to
                                    configure.
  --pad <name>                      Set the name of the tablet pad to
                                    configure.
  --button <index>                  Set the index of the stylus button or pad
                                    button to configure.
  --left-handed                     Query the left-handed mode of the tablet.
  --orientation                     Query the orientation of the tablet.
  --output-name                     Query the output name of the tablet.
  --binding                         Queries the keybinding of the button.
  --pressure-range-min              Queries the minimum value of the pressure
                                    range.
  --pressure-range-max              Queries the maximum value of the pressure
                                    range.
  --input-area                      Queries the logical input area of the
                                    stylus.
  --output-area                     Queries the output area on the mapped
                                    screen.
  --pressure-curve                  Queries the pressure curve of the stylus.
  --set-left-handed <on/off>        Sets the left-handed mode of the tablet.
  --set-orientation <orientation>   Sets the orientation of the tablet.
  --set-output-name <output-name>   Sets the output name of the tablet.
  --set-binding <keybind>           Sets the keybinding of the button.
  --set-pressure-range-min <value>  Sets the minimum value of the pressure
                                    range.
  --set-pressure-range-max <value>  Sets the maximum value of the pressure
                                    range.
  --set-input-area <value>          Sets the logical input area of the pen.
  --set-output-area <value>         Sets the output area of the mapped screen.
  --set-pressure-curve <value>      Sets the pressure curve of the stylus.
  --export-config                   Export the current config of this device as
                                    a list of commands.
  -h, --help                        Displays help on commandline options.
  --help-all                        Displays help, including generic Qt
                                    options.
  -v, --version                     Displays version information.
  --author                          Show author information.
  --license                         Show license information.
  --desktopfile <file name>         The base file name of the desktop entry for
                                    this application.
```

First you'll want to query the name of the stylus or pad to configure, since that's used for most options.

```shell
$ ktabletconfig --list-stylus
UGTABLET 21.5 inch PenDisplay Stylus
```

A common scenario is to script the tablet to switch to a different display. First you'll need to figure out the names of your connectors via `kscreen-doctor -o`. Then plug that into `ktabletconfig`.

```shell
$ ktabletconfig --stylus "UGTABLET 21.5 inch PenDisplay Stylus" --set-output-name "HDMI-1" 
```

If you want to configure keybinds, set the `--button` option to select which button to configure. To set the first stylus button to "E", do something like this:

```shell
$ ktabletconfig --stylus "UGTABLET 21.5 inch PenDisplay Stylus" --button 0 --set-binding "Key,E" 
```

See the information below for what you can stick into `--set-binding`.

## Rebinds

The supported options in KDE Plasma for rebinds are:

* Keys
  * Format: `Key,[keys]`
  * Examples: `Key,A`, `Key,Ctrl+Alt`
* Mouse buttons
  * Format: `Key,[mouse button code],[modifiers]`
  * Examples: `Mouse,272` (left-click), `Mouse,274` (middle-click), `Mouse,273` (right-click)
* Tablet tool buttons
  * Format: `TabletToolButton,[pen button code]`
  * Examples: `TabletToolButton,331` (first button), `TabletToolButton,331` (second button), `TabletToolButton,333` (third button)
* Disabled
  * Format: `Disabled`

It's best to rebind these in the Drawing Tablet KCM first, and then read them back with the `--binding` option.

## Exporting Config

You may want to save the current configuration of your stylus or pad. For example, to move it between machines or to load a specific workflow. You can export the configuration by using `--export-config`, which will give you a list of commands to restore your current configuration. This can be put into a shell script, for example.

```shell
$ ktabletconfig --pad "UGTABLET 21.5 inch PenDisplay Pad" --export-config
ktabletconfig --pad "UGTABLET 21.5 inch PenDisplay Pad" --button 0 --set-binding "Key,B"
ktabletconfig --pad "UGTABLET 21.5 inch PenDisplay Pad" --button 1 --set-binding "Key,E"
ktabletconfig --pad "UGTABLET 21.5 inch PenDisplay Pad" --button 2 --set-binding "TabletToolButton,332"
...
```

## Requirements

* KDE Plasma Desktop 6.3 or higher
  * Older versions of Plasma are unsupported. Only the Wayland session is supported.

## Get It

**Note:** The COPR is currently out of date. Please build it locally instead.

Builds are available for Fedora users through
my [personal COPR](https://copr.fedorainfracloud.org/coprs/redstrate/personal/):

```shell
sudo dnf copr enable redstrate/personal 
sudo dnf install ktabletconfig
```

## Notes

ktabletconfig intentionally does not support the same CLI interface as xsetwacom/gsetwacom, nor is it planned to. You will have to adapt your scripts.

Unlike xsetwacom, all modifications are **persistent** and will survive logouts/reboots etc. Modified settings will show up in the KCM and vice versa, so keep that in mind.

## Licensing and Credits

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This project is licensed under the GNU General Public License 3. This project uses some code from [Aleix Pol's kwin-inputdevice](https://invent.kde.org/apol/kwin-inputdevice).
