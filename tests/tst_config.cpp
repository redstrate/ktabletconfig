// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#include <QtTest>

#include "config.h"

class ConfigTest final : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase()
    {
        // Ensure that we don't mess up a real system when running this
        QStandardPaths::setTestModeEnabled(true);
    }

    void testStylusKeys()
    {
        writeStylusKey(QStringLiteral("TestDevice"), 0, QStringLiteral("A"));
        QCOMPARE(readStylusKey(QStringLiteral("TestDevice"), 0), QStringLiteral("A"));
    }

    void testPadKeys()
    {
        writePadKey(QStringLiteral("TestDevice"), 0, QStringLiteral("A"));
        QCOMPARE(readPadKey(QStringLiteral("TestDevice"), 0), QStringLiteral("A"));
    }
};

QTEST_MAIN(ConfigTest)
#include "tst_config.moc"
