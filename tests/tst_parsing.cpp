// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#include <QtTest>

#include "parsing.h"

class ParsingTest final : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testIntParsing_data()
    {
        QTest::addColumn<QString>("arg");
        QTest::addColumn<std::optional<int>>("expectedResult");

        QTest::addRow("Positive") << QStringLiteral("5") << std::optional{5};
        QTest::addRow("Negative") << QStringLiteral("-5") << std::optional{-5};
        QTest::addRow("Double Digits") << QStringLiteral("55") << std::optional{55};
        QTest::addRow("Alphabet") << QStringLiteral("A") << std::optional<int>{std::nullopt};
        QTest::addRow("Nothing") << QStringLiteral("") << std::optional<int>{std::nullopt};
    }

    void testIntParsing()
    {
        QFETCH(QString, arg);
        QFETCH(std::optional<int>, expectedResult);

        QCOMPARE(getIntOption(arg), expectedResult);
    }

    void testBoolParsing_data()
    {
        QTest::addColumn<QString>("arg");
        QTest::addColumn<std::optional<bool>>("expectedResult");

        QTest::addRow("True") << QStringLiteral("true") << std::optional{true};
        QTest::addRow("False") << QStringLiteral("false") << std::optional{false};
        QTest::addRow("Numeric") << QStringLiteral("55") << std::optional<bool>{std::nullopt};
        QTest::addRow("Alphabet") << QStringLiteral("A") << std::optional<bool>{std::nullopt};
        QTest::addRow("Nothing") << QStringLiteral("") << std::optional<bool>{std::nullopt};
    }

    void testBoolParsing()
    {
        QFETCH(QString, arg);
        QFETCH(std::optional<bool>, expectedResult);

        QCOMPARE(getBoolOption(arg), expectedResult);
    }

    void testOrientationParsing_data()
    {
        QTest::addColumn<QString>("arg");
        QTest::addColumn<std::optional<int>>("expectedResult");

        QTest::addRow("Default") << QStringLiteral("default") << std::optional{0};
        QTest::addRow("Portrait") << QStringLiteral("portrait") << std::optional{1};
        QTest::addRow("Landscape") << QStringLiteral("landscape") << std::optional{2};
        QTest::addRow("Inverted Portrait") << QStringLiteral("inverted-portrait") << std::optional{3};
        QTest::addRow("Inverted Landscape") << QStringLiteral("inverted-landscape") << std::optional{4};

        QTest::addRow("Numeric") << QStringLiteral("55") << std::optional<int>{std::nullopt};
        QTest::addRow("Alphabet") << QStringLiteral("A") << std::optional<int>{std::nullopt};
        QTest::addRow("Nothing") << QStringLiteral("") << std::optional<int>{std::nullopt};
    }

    void testOrientationParsing()
    {
        QFETCH(QString, arg);
        QFETCH(std::optional<int>, expectedResult);

        QCOMPARE(getOrientationOption(arg), expectedResult);
    }

    void testRectParsing_data()
    {
        QTest::addColumn<QString>("arg");
        QTest::addColumn<std::optional<QRectF>>("expectedResult");

        QTest::addRow("no decimals") << QStringLiteral("1,0,1,0") << std::optional{QRectF{1, 0, 1, 0}};
        QTest::addRow("decimals") << QStringLiteral("1.5,0.5,1,0.5") << std::optional{QRectF{1.5, 0.5, 1, 0.5}};
        QTest::addRow("too many values") << QStringLiteral("1.5,0.5,1,0.5,3.5") << std::optional<QRectF>{std::nullopt};
        QTest::addRow("too little values") << QStringLiteral("1.5,0.5") << std::optional<QRectF>{std::nullopt};
        QTest::addRow("empty") << QStringLiteral("") << std::optional<QRectF>{std::nullopt};
    }

    void testRectParsing()
    {
        QFETCH(QString, arg);
        QFETCH(std::optional<QRectF>, expectedResult);

        QCOMPARE(getRectOption(arg), expectedResult);
    }
};

QTEST_MAIN(ParsingTest)
#include "tst_parsing.moc"
