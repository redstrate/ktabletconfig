// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#include "config.h"

#include <KConfigGroup>
#include <KSharedConfig>

/**
 * @brief Converts from 0, 1, 2 to the real values of BTN_STYLUS,BTN_STYLUS2, and BTN_STYLUS3 respectively.
 */
int indexToRealStylusButton(const int index)
{
    switch (index) {
    default:
    case 0:
        return 0x14b; // BTN_STYLUS
    case 1:
        return 0x14c; // BTN_STYLUS2
    case 2:
        return 0x149; // BTN_STYLUS3
    }
}

QString readStylusKey(const QString &deviceName, const int index)
{
    const auto generalGroup = KSharedConfig::openConfig(QStringLiteral("kcminputrc"))->group(QStringLiteral("ButtonRebinds"));
    const auto group = generalGroup.group(QStringLiteral("TabletTool")).group(deviceName);

    return group.readEntry(QString::number(indexToRealStylusButton(index)));
}

QString readPadKey(const QString &deviceName, const int index)
{
    const auto generalGroup = KSharedConfig::openConfig(QStringLiteral("kcminputrc"))->group(QStringLiteral("ButtonRebinds"));
    const auto group = generalGroup.group(QStringLiteral("Tablet")).group(deviceName);

    return group.readEntry(QString::number(index));
}

void writeStylusKey(const QString &deviceName, const int index, const QString &key)
{
    auto generalGroup = KSharedConfig::openConfig(QStringLiteral("kcminputrc"))->group(QStringLiteral("ButtonRebinds"));
    auto group = generalGroup.group(QStringLiteral("TabletTool")).group(deviceName);

    const auto keyName = QString::number(indexToRealStylusButton(index));
    if (key.isEmpty()) {
        group.deleteEntry(keyName, KConfig::Notify);
    } else {
        group.writeEntry(keyName, key, KConfig::Notify);
    }
}

void writePadKey(const QString &deviceName, const int index, const QString &key)
{
    auto generalGroup = KSharedConfig::openConfig(QStringLiteral("kcminputrc"))->group(QStringLiteral("ButtonRebinds"));
    auto group = generalGroup.group(QStringLiteral("Tablet")).group(deviceName);

    const auto keyName = QString::number(index);
    if (key.isEmpty()) {
        group.deleteEntry(keyName, KConfig::Notify);
    } else {
        group.writeEntry(keyName, key, KConfig::Notify);
    }
}
