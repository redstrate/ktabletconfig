// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#include "parsing.h"

#include <QList>

std::optional<int> getIntOption(const QString &value)
{
    bool ok = false;
    const int val = value.toInt(&ok);
    if (ok) {
        return val;
    }
    return std::nullopt;
}

std::optional<bool> getBoolOption(const QString &value)
{
    if (value == QStringLiteral("true")) {
        return true;
    }
    if (value == QStringLiteral("false")) {
        return false;
    }
    return std::nullopt;
}

std::optional<int> getOrientationOption(const QString &value)
{
    if (value == QStringLiteral("default")) {
        return 0;
    }
    if (value == QStringLiteral("portrait")) {
        return 1;
    }
    if (value == QStringLiteral("landscape")) {
        return 2;
    }
    if (value == QStringLiteral("inverted-portrait")) {
        return 3;
    }
    if (value == QStringLiteral("inverted-landscape")) {
        return 4;
    }

    return std::nullopt;
}

QString getBoolValue(const bool value)
{
    if (value) {
        return QStringLiteral("true");
    }
    return QStringLiteral("false");
}

std::optional<QString> getOrientationOption(const int value)
{
    switch (value) {
    case 0:
        return QStringLiteral("default");
    case 1:
        return QStringLiteral("portrait");
    case 2:
        return QStringLiteral("landscape");
    case 3:
        return QStringLiteral("inverted-portrait");
    case 4:
        return QStringLiteral("inverted-landscape");
    default:
        return std::nullopt;
    }
}

std::optional<QRectF> getRectOption(const QString &value)
{
    const auto values = value.split(QLatin1Char(','));
    if (values.length() != 4) {
        return std::nullopt;
    }

    QList<float> parsedValues;
    std::ranges::transform(std::as_const(values), std::back_inserter(parsedValues), [](const QString &value) {
        return value.toFloat();
    });

    return QRectF(parsedValues[0], parsedValues[1], parsedValues[2], parsedValues[3]);
}
