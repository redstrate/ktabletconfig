// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#pragma once

#include <QSharedPointer>
#include <QStringList>

class OrgKdeKWinInputDeviceInterface;

/**
 * @brief Returns a list of evdev devices.
 */
QStringList enumerateDevices();

/**
 * @brief Fetches the relevant KWin device from DBus named @p sysname.
 */
QSharedPointer<OrgKdeKWinInputDeviceInterface> fetchDevice(const QString &sysname);