// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#include <KAboutData>
#include <KLocalizedString>

#include "InputDevice_interface.h"
#include "config.h"
#include "devices.h"
#include "ktabletconfig-version.h"
#include "parsing.h"

QTextStream &operator<<(QTextStream &out, const QRectF &rect)
{
    out << rect.x() << "," << rect.y() << "," << rect.width() << "," << rect.height();
    return out;
}

QString toString(const QRectF &rect)
{
    QString str;
    QTextStream stream(&str);
    stream << rect;

    return str;
}

int main(int argc, char *argv[])
{
    QTextStream out(stdout);
    QTextStream err(stderr);

    QCoreApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("ktabletconfig");

    KAboutData about(QStringLiteral("ktabletconfig"),
                     i18n("KDE Tablet Configurator"),
                     QString::fromUtf8(KTABLETCONFIG_VERSION_STRING),
                     i18n("Configure tablets from the CLI"),
                     KAboutLicense::GPL_V3,
                     i18n("© 2024 Joshua Goins"));
    about.addAuthor(i18n("Joshua Goins"), i18n("Maintainer"), QStringLiteral("josh@redstrate.com"));
    about.addCredit(i18n("Aleix Pol i Gonzalez"), i18n("Wrote kwin-inputdevice which this is based off of"));
    about.setTranslator(i18nc("NAME OF TRANSLATORS", "Your names"), i18nc("EMAIL OF TRANSLATORS", "Your emails"));
    KAboutData::setApplicationData(about);

    QCommandLineParser parser;

    const QCommandLineOption optionListStylus(QStringLiteral("list-stylus"), i18n("List connected tablet stylus."));
    parser.addOption(optionListStylus);

    const QCommandLineOption optionListPad(QStringLiteral("list-pad"), i18n("List connected tablet pads."));
    parser.addOption(optionListPad);

    const QCommandLineOption stylusOption(QStringList() << QStringLiteral("stylus"), i18n("Set the name of the tablet stylus to configure."), i18n("name"));
    parser.addOption(stylusOption);

    const QCommandLineOption padOption(QStringList() << QStringLiteral("pad"), i18n("Set the name of the tablet pad to configure."), i18n("name"));
    parser.addOption(padOption);

    const QCommandLineOption buttonOption(QStringList() << QStringLiteral("button"),
                                          i18n("Set the index of the stylus button or pad button to configure."),
                                          i18n("index"));
    parser.addOption(buttonOption);

    const QCommandLineOption leftHandedOption(QStringList() << QStringLiteral("left-handed"), i18n("Query the left-handed mode of the tablet."));
    parser.addOption(leftHandedOption);

    const QCommandLineOption orientationOption(QStringList() << QStringLiteral("orientation"), i18n("Query the orientation of the tablet."));
    parser.addOption(orientationOption);

    const QCommandLineOption outputNameOption(QStringList() << QStringLiteral("output-name"), i18n("Query the output name of the tablet."));
    parser.addOption(outputNameOption);

    const QCommandLineOption bindingOption(QStringList() << QStringLiteral("binding"), i18n("Queries the keybinding of the button."));
    parser.addOption(bindingOption);

    const QCommandLineOption pressureRangeMinOption(QStringList() << QStringLiteral("pressure-range-min"),
                                                    i18n("Queries the minimum value of the pressure range."));
    parser.addOption(pressureRangeMinOption);

    const QCommandLineOption pressureRangeMaxOption(QStringList() << QStringLiteral("pressure-range-max"),
                                                    i18n("Queries the maximum value of the pressure range."));
    parser.addOption(pressureRangeMaxOption);

    const QCommandLineOption inputAreaOption(QStringList() << QStringLiteral("input-area"), i18n("Queries the logical input area of the stylus."));
    parser.addOption(inputAreaOption);

    const QCommandLineOption outputAreaOption(QStringList() << QStringLiteral("output-area"), i18n("Queries the output area on the mapped screen."));
    parser.addOption(outputAreaOption);

    const QCommandLineOption pressureCurve(QStringList() << QStringLiteral("pressure-curve"), i18n("Queries the pressure curve of the stylus."));
    parser.addOption(pressureCurve);

    const QCommandLineOption setLeftHandedOption(QStringList() << QStringLiteral("set-left-handed"),
                                                 i18n("Sets the left-handed mode of the tablet."),
                                                 i18n("on/off"));
    parser.addOption(setLeftHandedOption);

    const QCommandLineOption setOrientationOption(QStringList() << QStringLiteral("set-orientation"),
                                                  i18n("Sets the orientation of the tablet."),
                                                  i18n("orientation"));
    parser.addOption(setOrientationOption);

    const QCommandLineOption setOutputNameOption(QStringList() << QStringLiteral("set-output-name"),
                                                 i18n("Sets the output name of the tablet."),
                                                 i18n("output-name"));
    parser.addOption(setOutputNameOption);

    const QCommandLineOption setBindingOption(QStringList() << QStringLiteral("set-binding"), i18n("Sets the keybinding of the button."), i18n("keybind"));
    parser.addOption(setBindingOption);

    const QCommandLineOption setPressureRangeMinOption(QStringList() << QStringLiteral("set-pressure-range-min"),
                                                       i18n("Sets the minimum value of the pressure range."),
                                                       i18n("value"));
    parser.addOption(setPressureRangeMinOption);

    const QCommandLineOption setPressureRangeMaxOption(QStringList() << QStringLiteral("set-pressure-range-max"),
                                                       i18n("Sets the maximum value of the pressure range."),
                                                       i18n("value"));
    parser.addOption(setPressureRangeMaxOption);

    const QCommandLineOption setInputAreaOption(QStringList() << QStringLiteral("set-input-area"),
                                                i18n("Sets the logical input area of the pen."),
                                                i18n("value"));
    parser.addOption(setInputAreaOption);

    const QCommandLineOption setOutputAreaOption(QStringList() << QStringLiteral("set-output-area"),
                                                 i18n("Sets the output area of the mapped screen."),
                                                 i18n("value"));
    parser.addOption(setOutputAreaOption);

    const QCommandLineOption setPressureCurveOption(QStringList() << QStringLiteral("set-pressure-curve"),
                                                    i18n("Sets the pressure curve of the stylus."),
                                                    i18n("value"));
    parser.addOption(setPressureCurveOption);

    const QCommandLineOption exportConfigOption(QStringList() << QStringLiteral("export-config"),
                                                i18n("Export the current config of this device as a list of commands."));
    parser.addOption(exportConfigOption);

    about.setupCommandLine(&parser);
    parser.process(app);
    about.processCommandLine(&parser);

    const auto deviceSysNames = enumerateDevices();
    if (deviceSysNames.isEmpty()) {
        err << i18n("Failed to get a list of devices.") << Qt::endl;
        return -1;
    }

    if (parser.isSet(optionListStylus)) {
        for (const QString &sysname : deviceSysNames) {
            auto device = fetchDevice(sysname);
            if (device->tabletTool()) {
                out << device->name() << Qt::endl;
            }
        }

        return 0;
    }

    if (parser.isSet(optionListPad)) {
        for (const QString &sysname : deviceSysNames) {
            auto device = fetchDevice(sysname);
            if (device->tabletPad()) {
                out << device->name() << Qt::endl;
            }
        }

        return 0;
    }

    if (!parser.isSet(padOption) && !parser.isSet(stylusOption)) {
        err << i18n("Missing --stylus or --pad option, despite this option requiring it!") << Qt::endl;
        return -1;
    }

    QSharedPointer<OrgKdeKWinInputDeviceInterface> selectedDevice;

    for (const QString &sysname : deviceSysNames) {
        const auto device = fetchDevice(sysname);
        if (parser.isSet(stylusOption) && device->name() == parser.value(stylusOption)) {
            selectedDevice = device;
            break;
        }
        if (parser.isSet(padOption) && device->name() == parser.value(padOption)) {
            selectedDevice = device;
            break;
        }
    }

    if (!selectedDevice) {
        err << i18n("Invalid tablet stylus or pad name.") << Qt::endl;
        return -1;
    }

    if (parser.isSet(padOption)) {
        if (parser.isSet(bindingOption)) {
            const auto button = getIntOption(parser.value(buttonOption));
            if (!button) {
                err << i18n("Invalid value for --button!") << Qt::endl;
                return -1;
            }
            out << readPadKey(selectedDevice->name(), *button) << Qt::endl;
        }

        if (parser.isSet(setBindingOption)) {
            const auto key = parser.value(setBindingOption);
            const auto button = getIntOption(parser.value(buttonOption));
            if (!button) {
                err << i18n("Invalid value for --button!") << Qt::endl;
                return -1;
            }
            writePadKey(selectedDevice->name(), *button, key);
        }

        if (parser.isSet(exportConfigOption)) {
            const QString prelude = QStringLiteral("ktabletconfig --pad \"%1\"").arg(parser.value(padOption));
            QStringList commands;

            for (uint i = 0; i < selectedDevice->tabletPadButtonCount(); i++) {
                const auto key = readPadKey(parser.value(padOption), i);
                commands.push_back(QStringLiteral("%1 --button %2 --set-binding \"%3\"").arg(prelude).arg(i).arg(key));
            }

            out << commands.join(QLatin1Char('\n')) << Qt::endl;
        }
    } else {
        if (parser.isSet(leftHandedOption)) {
            out << getBoolValue(selectedDevice->leftHanded()) << Qt::endl;
        }

        if (parser.isSet(orientationOption)) {
            if (const auto orientation = getOrientationOption(selectedDevice->orientationDBus())) {
                out << *orientation << Qt::endl;
            } else {
                return -1;
            }
        }

        if (parser.isSet(outputNameOption)) {
            out << selectedDevice->outputName() << Qt::endl;
        }

        if (parser.isSet(bindingOption)) {
            const auto button = getIntOption(parser.value(buttonOption));
            if (!button) {
                err << i18n("Invalid value for --button!") << Qt::endl;
                return -1;
            }
            out << readStylusKey(selectedDevice->name(), *button) << Qt::endl;
        }

        if (parser.isSet(pressureRangeMinOption)) {
            out << selectedDevice->pressureRangeMin() << Qt::endl;
        }

        if (parser.isSet(pressureRangeMaxOption)) {
            out << selectedDevice->pressureRangeMax() << Qt::endl;
        }

        if (parser.isSet(inputAreaOption)) {
            out << selectedDevice->inputArea() << Qt::endl;
        }

        if (parser.isSet(outputAreaOption)) {
            out << selectedDevice->outputArea() << Qt::endl;
        }

        if (parser.isSet(pressureCurve)) {
            out << selectedDevice->pressureCurve() << Qt::endl;
        }

        if (parser.isSet(setLeftHandedOption)) {
            if (selectedDevice->supportsLeftHanded()) {
                const auto option = getBoolOption(parser.value(setLeftHandedOption));
                if (!option) {
                    err << i18n("Invalid value for --set-left-handed!") << Qt::endl;
                    return -1;
                }
                selectedDevice->setLeftHanded(*option);
            } else {
                err << i18n("Stylus does not support left-handed mode!") << Qt::endl;
                return -1;
            }
        }

        if (parser.isSet(setOutputNameOption)) {
            const auto option = parser.value(setOutputNameOption);
            if (option.isEmpty()) {
                err << i18n("Invalid value for --set-output-name!") << Qt::endl;
                return -1;
            }
            selectedDevice->setOutputName(option);
        }

        if (parser.isSet(setOutputNameOption)) {
            const auto option = parser.value(setOutputNameOption);
            selectedDevice->setOutputName(option);
        }

        if (parser.isSet(setBindingOption)) {
            const auto key = parser.value(setBindingOption);
            const auto button = getIntOption(parser.value(buttonOption));
            if (!button) {
                err << i18n("Invalid value for --button!") << Qt::endl;
                return -1;
            }
            writeStylusKey(selectedDevice->name(), *button, key);
        }

        if (parser.isSet(setPressureRangeMinOption)) {
            if (selectedDevice->supportsPressureRange()) {
                selectedDevice->setPressureRangeMin(parser.value(setPressureRangeMinOption).toFloat());
            } else {
                err << i18n("Stylus does not support setting the pressure range!") << Qt::endl;
            }
        }

        if (parser.isSet(setPressureRangeMaxOption)) {
            if (selectedDevice->supportsPressureRange()) {
                selectedDevice->setPressureRangeMax(parser.value(setPressureRangeMaxOption).toFloat());
            } else {
                err << i18n("Stylus does not support setting the pressure range!") << Qt::endl;
            }
        }

        if (parser.isSet(setInputAreaOption)) {
            if (selectedDevice->supportsInputArea()) {
                const auto rect = getRectOption(parser.value(setInputAreaOption));
                if (!rect) {
                    err << i18n("Invalid value for --set-input-area!") << Qt::endl;
                    return -1;
                }
                selectedDevice->setInputArea(rect.value());
            } else {
                err << i18n("Stylus does not support setting the input area!") << Qt::endl;
            }
        }

        if (parser.isSet(setOrientationOption)) {
            const auto orientation = getOrientationOption(parser.value(setOrientationOption));
            if (!orientation) {
                err << i18n("Invalid value for --set-orientation!") << Qt::endl;
                return -1;
            }
            selectedDevice->setOrientationDBus(orientation.value());
        }

        if (parser.isSet(setOutputAreaOption)) {
            const auto rect = getRectOption(parser.value(setOutputAreaOption));
            if (!rect) {
                err << i18n("Invalid value for --set-output-area!") << Qt::endl;
                return -1;
            }
            selectedDevice->setOutputArea(rect.value());
        }

        if (parser.isSet(setPressureCurveOption)) {
            const auto curve = parser.value(setPressureCurveOption);
            selectedDevice->setPressureCurve(curve);
        }

        if (parser.isSet(exportConfigOption)) {
            const QString prelude = QStringLiteral("ktabletconfig --stylus \"%1\"").arg(parser.value(stylusOption));
            QStringList commands;

            // output name
            commands.push_back(QStringLiteral("%1 --set-output-name \"%2\"").arg(prelude, selectedDevice->outputName()));

            // output area
            commands.push_back(QStringLiteral("%1 --set-output-area \"%2\"").arg(prelude, toString(selectedDevice->outputArea())));

            // input area
            if (selectedDevice->supportsInputArea()) {
                commands.push_back(QStringLiteral("%1 --set-input-area \"%2\"").arg(prelude, toString(selectedDevice->inputArea())));
            }

            // orientation
            commands.push_back(QStringLiteral("%1 --set-orientation \"%2\"").arg(prelude, getOrientationOption(selectedDevice->orientationDBus()).value()));

            // pressure curve
            commands.push_back(QStringLiteral("%1 --set-pressure-curve \"%2\"").arg(prelude, selectedDevice->pressureCurve()));

            // pressure range
            if (selectedDevice->supportsPressureRange()) {
                commands.push_back(QStringLiteral("%1 --set-pressure-range-min \"%2\"").arg(prelude).arg(selectedDevice->pressureRangeMin()));
                commands.push_back(QStringLiteral("%1 --set-pressure-range-max \"%2\"").arg(prelude).arg(selectedDevice->pressureRangeMax()));
            }

            // left-handed
            if (selectedDevice->supportsLeftHanded()) {
                commands.push_back(QStringLiteral("%1 --set-left-handed \"%2\"").arg(prelude).arg(selectedDevice->leftHanded()));
            }

            // bindings
            for (int i = 0; i < 3; i++) {
                const auto key = readStylusKey(parser.value(stylusOption), i);
                commands.push_back(QStringLiteral("%1 --button %2 --set-binding \"%3\"").arg(prelude).arg(i).arg(key));
            }

            out << commands.join(QLatin1Char('\n')) << Qt::endl;
        }
    }

    return 0;
}
