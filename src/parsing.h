// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#pragma once

#include <QString>

#include <QRect>
#include <optional>

/**
 * @brief Parse a command line argument into an integer.
 */
std::optional<int> getIntOption(const QString &value);

/**
 * @brief Parse a command line argument into an bool.
 */
std::optional<bool> getBoolOption(const QString &value);

/**
 * @brief Parse a command line argument into an orientation.
 */
std::optional<int> getOrientationOption(const QString &value);

/**
 * @brief Returns a true/false depending on the boolean value.
 */
QString getBoolValue(bool value);

/**
 * @brief Returns the orientation depending on the int value.
 */
std::optional<QString> getOrientationOption(int value);

/**
 * @brief Returns the rectdepending on multiple values.
 */
std::optional<QRectF> getRectOption(const QString &value);
