// SPDX-FileCopyrightText: 2024 Joshua Goins <josh@redstrate.com>
// SPDX-License-Identifier: GPL-3.0-only

#pragma once

#include <QString>

/**
 * @brief Reads the keybind set on the stylus from @p deviceName at button @p index.
 */
QString readStylusKey(const QString &deviceName, int index);

/**
 * @brief Reads the keybind set on the pad from @p deviceName at button @p index.
 */
QString readPadKey(const QString &deviceName, int index);

/**
 * @brief Sets the keybind set on the stylus from @p deviceName at button @p index to @p key.
 */
void writeStylusKey(const QString &deviceName, int index, const QString &key);

/**
 * @brief Sets the keybind set on the pad from @p deviceName at button @p index to @p key.
 */
void writePadKey(const QString &deviceName, int index, const QString &key);