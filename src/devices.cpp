// SPDX-FileCopyrightText: 2022 Aleix Pol Gonzalez <aleixpol@kde.org>
// SPDX-License-Identifier: GPL-3.0-only
// TODO: Aleix has no license assigned to the repository, assuming GPL. Fix before release.

#include "devices.h"

#include "InputDevice_interface.h"

QStringList enumerateDevices()
{
    const auto m_deviceManager = new QDBusInterface(QStringLiteral("org.kde.KWin"),
                                                    QStringLiteral("/org/kde/KWin/InputDevice"),
                                                    QStringLiteral("org.kde.KWin.InputDeviceManager"),
                                                    QDBusConnection::sessionBus(),
                                                    QCoreApplication::instance());
    const QVariant reply = m_deviceManager->property("devicesSysNames");
    if (reply.isValid()) {
        QStringList devicesSysNames = reply.toStringList();
        devicesSysNames.sort();

        return devicesSysNames;
    }
    return {};
}

QSharedPointer<OrgKdeKWinInputDeviceInterface> fetchDevice(const QString &sysname)
{
    auto ret = QSharedPointer<OrgKdeKWinInputDeviceInterface>::create(QStringLiteral("org.kde.KWin"),
                                                                      QStringLiteral("/org/kde/KWin/InputDevice/") + sysname,
                                                                      QDBusConnection::sessionBus());
    if (ret->sysName() != sysname) {
        return {};
    }
    return ret;
}
